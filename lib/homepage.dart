import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gas/editdata.dart';
import 'package:gas/tambahdata.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List _listData = [];
  bool _isLoading = true;

  Future _getData() async {
    String uri = "http://192.168.1.11:8080/";
    final response = await http.get(Uri.parse(uri));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        _listData = data;
        _isLoading = false;
      });
    }
  }

  _hapus(String id) async {
    String uri = "http://192.168.1.11:8080/delete?id=$id";
    final response = await http.delete(
      Uri.parse(uri),
    );
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  @override
  void initState() {
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
      ),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: _listData.length,
              itemBuilder: (context, index) {
                return Card(
                    child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              EditData(id: _listData[index]["id"]),
                        ));
                  },
                  child: ListTile(
                    title: Text(_listData[index]["name"]),
                    subtitle: Text(_listData[index]["email"]),
                    trailing: IconButton(
                      onPressed: () {
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              actions: [
                                ElevatedButton(
                                    onPressed: () {
                                      _hapus(_listData[index]["id"])
                                          .then((value) {
                                        if (value) {
                                          final snackBar =
                                              const SnackBar(content: Text("ok"));
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackBar);
                                        } else {
                                          final snackBar =
                                              const SnackBar(content: Text("fail"));
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackBar);
                                        }
                                      });
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => const HomePage(),
                                          ),
                                          (route) => false);
                                    },
                                    child: const Text("hapus")),
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text("batal"))
                              ],
                              content:
                                  const Text("apakah anda ingin menghapus data ini?"),
                            );
                          },
                        );
                      },
                      icon: const Icon(Icons.delete),
                    ),
                  ),
                ));
              },
            ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const TambahDataPage()));
        },
      ),
    );
  }
}
