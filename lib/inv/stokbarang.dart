import 'package:flutter/material.dart';

import '../tambahdata.dart';

class StokBarang extends StatefulWidget {
  const StokBarang({Key? key}) : super(key: key);

  @override
  State<StokBarang> createState() => _StokBarangState();
}

class _StokBarangState extends State<StokBarang> {
  @override
  Widget build(BuildContext context) {
    List _listDataBarang = [
      {
        "sku": "SUK-001",
        "nama_barang": "Sukro",
        "harga": 20000,
        "modal": 10000
      },
      {
        "sku": "SUK-002",
        "nama_barang": "Sukro",
        "harga": 20000,
        "modal": 10000
      },
      {"sku": "SUK-003", "nama_barang": "Sukro", "harga": 20000, "modal": 10000}
    ];
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return MaterialApp(
      home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
                bottom: TabBar(tabs: [
                  Tab(text: "Semua Barang"),
                  Tab(text: "Stok Menipis"),
                ]),
                title: Text("Stok Barang")),
            body: TabBarView(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Container(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          children: [
                            const Expanded(
                              child: TextField(
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    fillColor: Color(0xFFF3F3F3),
                                    filled: true,
                                    prefixIcon: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: CircleAvatar(
                                          backgroundColor: Color(0xFFFFEF97),
                                          child: Icon(Icons.search)),
                                    ),
                                    hintText: "Cari barang"),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Column(
                                // mainAxisSize: MainAxisSize.min,
                                // mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Icon(Icons.filter_list),
                                  Text(
                                    "Urutkan",
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: ListView.builder(
                            itemCount: 1000,
                            itemBuilder: (context, index) {
                              return Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "SUK-001",
                                          style: TextStyle(fontSize: 10),
                                        ),
                                        Text(
                                          "Bensin",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        Text(
                                          "Rp 10.000",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Color(0xFFB7F4E5)),
                                        ),
                                        Text(
                                          "Harga Pokok : Rp 5.000",
                                          style: TextStyle(fontSize: 10),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Text("Stok Produk"),
                                        Row(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 8.0),
                                              child: Text("5"),
                                            ),
                                            Icon(
                                              Icons.edit,
                                              size: 10,
                                            ),
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                ListView.builder(
                  itemCount: _listDataBarang.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(15),
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Text(_listDataBarang[index]["sku"]),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Text(
                                        _listDataBarang[index]["nama_barang"]),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Text(_listDataBarang[index]["harga"]
                                        .toString()),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Text(_listDataBarang[index]["modal"]
                                        .toString()),
                                  )
                                ],
                              ),
                              Column(
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Stok Produk"),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                // Icon(Icons.directions_car),
                // Icon(Icons.directions_transit),
              ],
            ),
            floatingActionButton: FloatingActionButton.extended(
              label: Row(
                children: const [
                  Icon(Icons.add),
                  Text("Tambah Barang"),
                ],
              ),
              onPressed: () {
                showModalBottomSheet<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return SizedBox(
                      height: height,
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        // mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          TextFormField(
                            decoration: InputDecoration(
                                hintText: "email",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20))),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "email tidak boleh kosong";
                              }
                              return null;
                            },
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                                hintText: "email",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20))),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "email tidak boleh kosong";
                              }
                              return null;
                            },
                          ),
                          ElevatedButton(
                            child: const Text('Close BottomSheet'),
                            onPressed: () => Navigator.pop(context),
                          ),
                        ],
                      ),
                    );
                  },
                );
              },
            ),
          )),
    );
  }
}
