import 'dart:convert';

import 'package:gas/homepage.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class EditData extends StatefulWidget {
  final String id;

  const EditData({Key? key, required this.id}) : super(key: key);

  @override
  State<EditData> createState() => _EditDataState();
}

class _EditDataState extends State<EditData> {
  final formKey = GlobalKey<FormState>();
  Map _detailData={};
  TextEditingController id = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();

  _update() async {
    Map payload = {"name": name.text, "email": email.text, "id": id.text};
    String uri = "http://192.168.1.11:8080/update";
    final response = await http.post(Uri.parse(uri),
        headers: {"Content-Type": "application/json"},
        body: json.encode(payload));
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  Future _getDataById(String id) async {
    String uri = "http://192.168.1.11:8080/$id";
    final response = await http.get(Uri.parse(uri));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        _detailData = data;
      });
    }
  }

  @override
  void initState() {
    _getDataById(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    id.text = _detailData["id"];
    name.text = _detailData["name"];
    email.text = _detailData["email"];
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Data"),
      ),
      body: Form(
        key: formKey,
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              TextFormField(
                controller: name,
                decoration: InputDecoration(
                    hintText: "name",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20))),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "name tidak boleh kosong";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: email,
                decoration: InputDecoration(
                    hintText: "email",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20))),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "email tidak boleh kosong";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      _update().then((value) {
                        if (value) {
                          const snackBar = SnackBar(content: Text("ok"));
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        } else {
                          const snackBar = SnackBar(content: Text("fail"));
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      });
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const HomePage(),
                          ),
                          (route) => false);
                    }
                  },
                  child: const Text("Update"))
            ],
          ),
        ),
      ),
    );
  }
}
